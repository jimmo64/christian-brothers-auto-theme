﻿module.exports = function(grunt){

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
		
		sass: {
			dist: {
				files: {
					'style/style.css' : 'sass/style.scss'
				}
			}
		},
		watch: {
			css: {
				files: '**/*.scss',
				tasks: ['sass']
			}
		}
		
		
		//Move to dev **be connected to VPN/network**
		copy: {
		  files: {
			cwd: 'build/css/',
			src: ['**',  '!**/fonts/**'],
			dest: 'C:/xampp/htdocs/ips4-0-5/themes/2/css/core/front/custom/',
			//dest: 'C:/Users/jimmo/Documents/test/',
			expand: true 
		  }
		}
	
    });
	
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-copy');

    grunt.registerTask('default', ['copy']);
	
	

};